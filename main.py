from bs4 import BeautifulSoup
import classes
import base64
import plotly.graph_objs as go
import plotly.offline as offline
import geopy.distance
import googlemaps
import time
import json
from pprint import pprint


mapbox = str(base64.b64decode(b'cGsuZXlKMUlqb2liV0YwZEdobGQyaGhiV2xzZEc5dUlpd2lZU0k2SW1OcU5HNWxiamM1Y2pGbU4zRXpNVzFvZDJ3MGJuQmlaWEFpZlEuR3pqdF8xSTJTa2hWdFBoc25USVB5UQ=='), 'utf-8')
google = 'AIzaSyCzLdMw0nEUnnf-UTLNZfxeQXcHWtFFymU'


food_classifications = [
    'cafe',
    'restaurant',
    'fast_food',
    'bar',
    'pub',
    'pharmacy',
    'ice_cream',
]
street_classifications = [
    'residential',
    'secondary',
]


def get_restaurants():
    gmaps = googlemaps.Client(key=google)
    data = []
    result = googlemaps.places.places_nearby(client=gmaps, location='41.883208, -87.631691', radius=10000, type='food')
    while True:
        data.extend(result['results'])

        if result.get('next_page_token') is not None:
            time.sleep(3)
            result = googlemaps.places.places_nearby(client=gmaps, page_token=result.get('next_page_token'))
            pprint(result.get('next_page_token'))
        else:
            break
    json.dump(data, open('restaurants.json', 'w'))


def parse_restaurants():
    data = json.load(open('restaurants.json', 'r'))
    restaurant_locations = [r['geometry']['location'] for r in data]
    print(len(restaurant_locations))
    exit()
    return [classes.restaurant(classes.location(r['lat'], r['lng'])) for r in restaurant_locations]


def convert_osm_data():
    data = {
        'nodes': [],
        'ways': [],
    }
    map = BeautifulSoup(open('map', 'rb').read().decode('utf-8'), 'lxml')
    for node in map.findAll('node'):
        data['nodes'].append(classes.osm_node(node))
    for way in map.findAll('way'):
        data['ways'].append(classes.osm_way(way))
    return data


def extract_osm_data(raw_data):
    # TODO when limit_copy becomes less than zero, find the point along the line of the two points with the right distance to make it zero. (it'll be less than zero, so do it from the not included point)
    # TODO split into two functions when it hits an intersection
    def offlimits_area(location, street, streets, intersections, limit=200, type="street"):
        def offlimits_area_circle(location, limit=200, accuracy=36):
            p = geopy.Point(location.lat, location.lon)
            d = geopy.distance.VincentyDistance(feet=limit)
            circle = [d.destination(point=p, bearing=x) for x in range(0, 360, 360 // accuracy)]
            return [classes.location(c[0], c[1]) for c in circle]

        def offlimits_area_street(location, street, streets, intersections, limit=200):
            paths = [[location], [location]]
            index = None
            for i, p in enumerate(street.path):
                if p == location:
                    index = i
                    break
            limit_copy = limit
            for i in reversed(range(index)):
                limit_copy = limit_copy - geopy.distance.distance((street.path[i].lat, street.path[i].lon), (street.path[i + 1].lat, street.path[i + 1].lon)).ft
                if limit_copy < 0:
                    break
                else:
                    paths[0].append(street.path[i])

            limit_copy = limit
            for i in range(index + 1, len(street.path)):
                limit_copy = limit_copy - geopy.distance.distance((street.path[i - 1].lat, street.path[i - 1].lon), (street.path[i].lat, street.path[i].lon)).ft
                if limit_copy < 0:
                    break
                else:
                    paths[1].append(street.path[i])
            return paths

        if type == 'street':
            return offlimits_area_street(location, street, streets, intersections, limit)
        if type == 'circles':
            return offlimits_area_circle(location, limit=limit)

    def format_json(data):
        base = {
            'type': 'FeatureCollection',
            'features': [],
        }
        for d in data:
            d_formatted = {
                'geometry': {
                    'type': 'MultiPolygon',
                    'coordinates': [[[[l.lon, l.lat] for l in d]]],
                },
                'type': 'Feature',
            }
            base['features'].append(d_formatted)
        return base

    restaurants = parse_restaurants()
    streets = []
    offlimits_paths = []
    intersections = {}

    ref_dict = {x.id: x.location for x in raw_data['nodes']}

    for way in raw_data['ways']:
        street = way.characteristics.get('highway', None)
        if street in street_classifications:
            streets.append(classes.street(way, ref_dict))
        elif street is not None:
            pass

    for street in streets:
        for p in street.path:
            if p in intersections:
                intersections[p].append(street)
            else:
                intersections[p] = [street]

    # for ref, streets in list(intersections.items()):
    #     if len(streets) == 1:
    #         intersections.pop(ref)

    for r in restaurants:
        r.map_to_street(streets)
        if False:
            offlimits_paths.extend(offlimits_area(r.street_location, r.street_object, streets, intersections, type='circles'))
        if True:
            offlimits_paths.append(offlimits_area(r.location, r.street_object, streets, intersections, type='circles'))
    if True:
        offlimits_paths = format_json(offlimits_paths)
    return {
        'restaurants': restaurants,
        'offlimits': offlimits_paths,
    }


def create_map(data, offlimits_type):
    plotly_data = [
        go.Scattermapbox(
            lat=[x.location.lat for x in data['restaurants']],
            lon=[x.location.lon for x in data['restaurants']],
            mode="markers",
            marker=dict(
                size=5,
                color="green",
            )
        )
    ]
    if offlimits_type == 'street':
        plotly_data.append(
            go.Scattermapbox(
                lat=[x.lat for x in path],
                lon=[x.lon for x in path],
                mode="lines",
                line=dict(
                    color="red",
                    width=5,
                ),
            )
            for path in data['offlimits']
        )
    layout = go.Layout(
        title="Work in Progress",
        autosize=True,
        showlegend=False,
        mapbox=dict(
            accesstoken=mapbox,
            layers=[{
                'sourcetype': 'geojson',
                'source': data['offlimits'],
                'type': 'fill',
                'color': 'red',
                'opacity': .2,
            }],
            style='light',
            zoom=14,
            center=dict(
                lat=41.885,
                lon=-87.629,
            ),
        ),
    )
    fig = dict(data=plotly_data, layout=layout)
    offline.plot(fig, image='png')


if __name__ == '__main__':
    raw_data = convert_osm_data()
    data = extract_osm_data(raw_data)
    create_map(data, 'circle')
