class osm_node:
    def __init__(self, node):
        self.id = node['id']
        self.location = location(node['lat'], node['lon'])
        self.characteristics = {}
        for tag in node.findAll('tag'):
            self.characteristics[tag['k']] = tag['v']

    def __repr__(self):
        newline = '\n'
        characteristics = [f"\t{x}: {y}" for x, y in self.characteristics.items()]
        return f'\nId: {self.id}\nLocation: {self.location}\nCharacteristics:\n{newline.join(characteristics)}\n'


class osm_way:
    def __init__(self, way):
        self.id = way['id']
        self.characteristics = {
            'oneway': 'no'
        }
        self.path = [nd['ref'] for nd in way.findAll('nd')]
        for tag in way.findAll('tag'):
            self.characteristics[tag['k']] = tag['v']

    def __repr__(self):
        newline = '\n'
        characteristics = [f"\t{x}: {y}" for x, y in self.characteristics.items()]
        return f"\nId: {self.id}\nPath References: {{{', '.join(self.path)}}}\nCharacteristics: \n{newline.join(characteristics)}\n"


class location:
    def __init__(self, lat, lon):
        self.lat = float(lat)
        self.lon = float(lon)

    def map_to_street(self, streets_list):
        best_street = streets_list[0]
        best_distance = min([p - self for p in streets_list[0].path])
        best_index = 0

        for street in streets_list[1:]:
            distance = min([p - self for p in street.path])
            if distance < best_distance:
                best_street = street
                best_distance = distance
        nearest_point = min(best_street.path, key= lambda x: x - self)
        return nearest_point, best_street

    def __repr__(self):
        return f'{{lat: {self.lat}, lon: {self.lon}}}'

    def __sub__(self, other):  # returns the standard distance
        return ((self.lat - other.lat) ** 2 + (self.lon - other.lon) ** 2) ** .5


class street:
    def __init__(self, way, ref_dict):
        self.characteristics = way.characteristics
        self.path = [ref_dict[x] for x in way.path]

    def __repr__(self):
        newline = '\n'
        characteristics = [f"\t{x}: {y}" for x, y in self.characteristics.items()]
        return f"Path: {{{', '.join([repr(x) for x in self.path])}}}\nCharacteristics: \n{newline.join(characteristics)}\n"


class restaurant:
    def __init__(self, location):
        self.location = location
        self.name = 'N/A'
        self.food_type = 'N/A'
        self.street_location = None

    def map_to_street(self, streets_list):
        self.street_location, self.street_object = self.location.map_to_street(streets_list)

    def __repr__(self):
        return f'\nName: {self.name}\nLocation: {self.location}\nStreet Location: {self.street_location}\nType:{self.food_type}\n'
